# Email und Office

## Mail & Office Suite

### Hosted Service

- [**mailbox.org**](https://mailbox.org/de/)
- [**ProtonMail**](https://protonmail.com/de/pri)
- [**Mailfence**](https://mailfence.com/)

### Self Hosted
- [**Mail-in-a-Box**](https://mailinabox.email/)
- [**Mailcow**](https://mailcow.email/)

### FAQ
- [**KATEGORIE: TIPPS**](https://blog.mailfence.com/de/category/tipps/page/6/)

<details>
    <summary>Click to expand</summary>
    Bls bla Bla
</details>

## Buchhaltung, Personalwesen & Finanzen

- [**Swiss21.org**](https://swiss21.org/): 
